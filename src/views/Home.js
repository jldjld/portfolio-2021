import { React } from "react";
import { Link } from "react-router-dom";
import ProjectsList from '../components/Cards/ProjectsList';
import linkedin from "../assets/in.webp";
import gitlab from "../assets/gitlab-icon-1-color-white-rgb.webp";
import projects from "../data";
import "./Home.css";


function Home() {

  //useEffect(() => {
   // gsap.to( '.work',{ duration: 0.9, y: 80,  stagger: 0.5,ease: "back"} );
  //});


  return (
    
    <div className="container-fluid p-0 m-0">
    {/** BANNER BLOC */}
    <div className="row banner p-0 m-0">
      <div className="col-12">
        <h1 className="titre work text-left ml-5"> 
          Hello, i'm <Link to="/about" className="link-jld">Joan</Link>
        </h1>
        <div className="description text-left ml-5">
            <h2 id="title2" className="work">I'm a junior web developer</h2>
        </div>
        <div className="scrollDown text-center">
            <a href="#projects" className="btn">v</a>
        </div>
        <div className="icons text-right align-items-center justify-content-center position-fixed">
          <a href="https://www.linkedin.com/in/jld-simplon/">
              <img className="linkedin" src={linkedin} width="30px" alt="jld_linkedin"/>
          </a>
          <br/>
          <a href="https://gitlab.com/jldjld">
              <img className="gitlab" src={gitlab} width="30px" alt="jld_gitlab"/>
          </a>
        </div>
      </div>
    </div>
    {/** PROJECTS */}
    <div className="row p-0 m-0">
      <div className="col-12 projects align-items-center mt-lg-5 pl-5 pb-4">
        <h3 id="projects">Projects</h3>
        <p className="tagline mr-3">Professionnal, personal and school applications</p>
      </div>

        <ProjectsList projects={projects} />

    </div>
  </div>
  );
}

export default Home;

