import React from "react";
import "./About.css";
import jldIcon from "../assets/jld_icon.gif";

function About() {
  return (
    <div className="blackBg p-0 m-0">
    <div className="row aboutMeTxt d-flex align-items-center justify-content-center text-justify pt-5 m-0">
        <div className="intro col-10 col-md-8 mt-5 pt-5 pt-md-3">
            <p >Hello I am <a className="linkedin-url" href="https://www.linkedin.com/in/jld-simplon/">Joan</a>,<br/> 
            PHP / Java back-end Developer.<br/>
            <span>Now</span> learning more about <span>Java</span>, 
            I would love to get experience on theses technologies. <br/> Feel free to <a href="mailto:jldsimplon@gmail.com">contact me</a> </p>
            <hr className="pb-2 pb-md-5"/>

        </div>


        <div className="tech-skills d-md-flex justify-content-between col-8 mt-3 mb-3">
        
            <div className="col-12 col-md-3">
                <ul className=" col-12 mt-3">
                    <p>Current focus</p>
                    <li className="skills">Java</li>
                </ul>
                <ul className=" col-12 mt-3">
                    <p>worked with</p>
                    <li className="skills">SpringBoot</li>
                    <li className="skills">PHP 8.1</li>
                    <li className="skills">Timber / Twig</li>
                    <li className="skills">mySql</li>
                    <li className="skills">Docker</li>
                    <li className="skills">S3 AWS</li>
                    <li className="skills">Bootstrap</li>
                    <li className="skills">HTML 5/ CSS 3 / SASS</li>
                </ul>
            </div>
            <div className="col-12 col-md-2 mt-3">
                <ul>
                    <p>OTHER SKILLS</p>
                    <li className="skills">Adobe Illustrator CC</li>
                    <li className="skills">Photoshop CC</li>
                </ul>
            </div>
            <div className="col-12 col-md-2 mt-3">
                <ul>
                    <p>Plan to learn</p>
                    <li className="skills">Angular</li>
                    <li className="skills">React JS</li>
                </ul>
            </div>
            <div className="col-12 col-md-2 jld-icon d-flex justify-content-center">
            <img src={jldIcon} alt="jld_icon" className="img-fluid rounded-circle p-0 m-0"/>
        </div>  
        </div>
    </div>
  </div>
  );
}

export default About;

