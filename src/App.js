import './App.css';
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";
import Nav from './components/Nav/Nav.js';
import Home from './views/Home.js';
import About from './views/About.js';
import Footer from './components/Footer/Footer';
import ScrollToTop from './assets/js/ScrollToTop';
import { useEffect } from 'react';
import MouseCircle from './assets/js/MouseCircle';

function App() {

  useEffect(() => {
    < MouseCircle />
  });

  return (
    <Router>
      {/** Correct the scroll history saved by browser */}
      <ScrollToTop />
      <div className="App">
        <header>
        <Nav></Nav>
        </header>
        <Switch>
            <Route exact path="/">
              <Home />
            </Route>
            <Route path="/about">
              <About about="true" />
            </Route>
        </Switch>
        <Footer/>
        {/** CURSOR */}
        <div className="circle" >
            <svg>
                <circle cx="50" cy="50" r="10" strokeWidth="3" fill="white" />
            </svg>
        </div>
      </div>
    </Router>
  );
};

export default App;
