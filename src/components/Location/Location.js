import { useLocation } from "react-router-dom";

const Location = () => {

    const { pathname } = useLocation();

    return (

        { pathname }

    );
};

export default Location;