
import React, { useRef } from "react";
import { Link, NavLink, useLocation } from 'react-router-dom';
import jldIcon from '../../assets/jldicon.webp';
import jldWhiteIcon from '../../assets/jldwhiteicon.webp';

const Nav = () => {

    const { pathname } = useLocation();

    const navLinks = useRef();

    return (
        
    <nav className={ pathname !== '/about' ? "navbar navbar-expand-lg navbar-light fixed-top" : "navbar navbar-expand-lg fixed-top navbar-dark" }>

        <Link to="/"><img src={ pathname !== '/about' ? jldIcon : jldWhiteIcon } width="50" height="40" alt="jld-icon"/></Link>
        
        <button id="navBtn" className="navbar-toggler collapsed custom-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span className="navbar-toggler-icon"></span>
        </button>

        <div className="collapse navbar-collapse" id="navbarNav" ref={navLinks}>
            <ul className="navbar-nav ml-auto text-center">
                <li className="nav-item">
                    <NavLink className="nav-link" to="/">HOME</NavLink>
                </li>
                <li className="nav-item">
                    <NavLink className="nav-link" to="/about">ABOUT</NavLink>
                </li>
            </ul>
        </div>
      </nav>
    );
};

export default Nav;