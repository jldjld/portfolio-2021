import React from "react";
import { useLocation, NavLink } from "react-router-dom";
import "./Footer.css";

const Footer = () => {

    const { pathname } = useLocation();

    return (

    <footer className={ pathname !== "/about" ? "" : "about-footer" }>

    <div className="row justify-content-center align-items-center beforefooter p-0 m-0 pt-4">
        <div className="col-12 underline">
        </div>
    </div>

    <div className="row p-0 m-0">
        <div className="col-12 footer d-flex text-left pt-5 mb-3">
        <div className="col-6">
                <ul className="bottommenu">
                    <li className="bottom-nav">
                        <h6>SITEMAP</h6>
                    </li>
                    <li className="bottom-nav">
                        <NavLink to="/">HOME</NavLink>
                    </li>
                    <li className="bottom-nav">
                        <NavLink to="/about">ABOUT</NavLink>
                    </li>
                </ul>
            </div>
            <div className="col-6">
                <ul className="bottommenu">
                    <li className="bottom-nav mt-3">
                        <h6>Say hi to me</h6>
                    </li>
                    <li className="bottom-nav">
                        <p>jldsimplon@gmail.com</p>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</footer>
 );
};

export default Footer;