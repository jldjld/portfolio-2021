import React from "react";
import "./ProjectsList.css";

const ProjectCard = (props) => {

    const projects = props.projects;

    const listProjects = projects.map((project) =>

        <div className={project.cname} key={project.cardTitle}>

        <a href={project.gitUrl ? project.gitUrl : project.projectUrl} className="project-urls pr-3">
            <div className="card pb-0 mb-0 pt-0 mt-0">
                <img src={project.cardImg} className="img-fluid" alt="project-card"/>
                <div className="card-header">
                    <div className="category">
                        <h2>{project.cardTitle}</h2> {project.cardCategory}
                        
                    </div>
                </div>
            </div>
        </a>

        <div className="card-content d-flex flex-column align-items-start">
            <div className="d-flex mt-3 mb-0 pb-0">
                <h5 className="pr-2">{project.cardTitle}</h5>
                <p className="experience p-0 m-0">- {project.cardCategory}</p>
            </div>

            <p className="tech">{project.cardDate}</p>
            <p className="projectDescription pb-0 mb-0">{project.cardDescription}</p>

            {/** If url is empty display a message */}
            <div className="d-flex project-urls pt-3 pb-3">
            {project.gitUrl 
            ? <a href={project.gitUrl} className="url pr-3">
                . Gitlab
            </a>
            : <p className="private pr-3">☺ Private Git repo</p>
            }

            {project.projectUrl
            ? <a href={project.projectUrl} className="url">
                . www
            </a>
            : <p className="private">. Has not been deployed</p>}
            </div>
        </div>

        <hr/>

    </div>
    
    )

    return (
        <div className="row d-flex justify-content-center m-0 p-0">

            {listProjects}

        </div>
    );
};

export default ProjectCard;