import { gsap } from "gsap";

const SlowLoading = 
gsap.from(".work", { duration: 1, opacity: 0, y: "random(-200, 200)", stagger: 0.5, ease: "back" })
;

export default SlowLoading;