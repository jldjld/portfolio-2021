import { gsap } from "gsap";

const MouseCircle = (e) => {
    gsap.to( '.circle', {
        duration: 0.3,
        css: {
            x: e.pageX,
            y: e.pageY
        }
    });
}

window.addEventListener( 'mousemove', MouseCircle );

export default MouseCircle;

