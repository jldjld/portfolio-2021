import uniden from "./assets/uniden.webp";
import panpan from "./assets/jsgamegif.gif";
import replace from "./assets/replace.webp";
import franceh from "./assets/franceh.webp";
import monitor from "./assets/data.webp";
import solar from "./assets/solar.webp";

export const projects = [
    { 
        cardTitle: "Uniden",
        cname:"work col-md-4 mt-3 ml-3 mr-3",
        cardImg:uniden,
        cardCategory:"Professionnal experience",
        cardDate:"PHP - WordPress - Timber JS - Docker / April 2021",
        cardDescription:"Showcase website for an industry group. Implemented functionnality: Import Investors data dynamically, using WP CLI methods and a cron script.",
        gitUrl:"",
        projectUrl:"https://www.uniden.fr/",
    },
    {
        cardTitle:"Forecast Weather",
        cname:"work col-md-4 mt-3 ml-3 mr-3",
        cardImg:solar,
        cardCategory:"Training project",
        cardDate:"React Native / July 2021",
        cardDescription:"Type a city, get current weather informations. Implemented functionnality: Display matching weather data, using a REST Api.",
        gitUrl:"https://gitlab.com/jldjld/solar-native",
        projectUrl:"",
    },
    {
        cname:"work col-md-4 mt-3 ml-3 mr-3",
        cardImg:monitor,
        cardTitle:"WP-Dashboard",
        cardCategory:"Apprenticeship project",
        cardDate:"PHP - WordPress - Timber JS - Docker / February 2021",
        cardDescription: "WordPress monitoring dashboard, to display an array of deployed WordPress projects, and get key informations like active WP and plugins version.\n Implemented functionnality: WP Plugin development, to initialize a REST Api route,\n protected by a token. Dashboard Theme to display updated data, every 24h a CRON script, gets wp connected projects informations. Theme is on a private Reposirtory, see the plugin here",
        gitUrl:"https://gitlab.com/jldjld/listener",
        projectUrl:"",
    },
    {
        cname:"work col-md-4 mt-3 mt-md-0 ml-3 mr-3 eslscaCard",
        cardImg:franceh,
        cardTitle:"France Hydrogène",
        cardCategory:"Professionnal experience",
        cardDate:"PHP - WordPress - Timber JS - Docker / September 2021",
        cardDescription: "Industrial group website,.\n Implemented functionnality: dynamic magazine builder, based on post dates, and categories.",
        gitUrl:"",           
        projectUrl:"https://www.france-hydrogene.org/",
    },
    {
        cname:"work col-md-4 mt-3 mt-md-0 ml-3 mr-3 replaceCard",
        cardImg:replace,
        cardTitle:"Replace",
        cardCategory:"Training project",
        cardDate:"Front-end Angular 7 - back-end Symfony 4 / March 2020",
        cardDescription:"An application to find eco-conscious alternatives for single use products.\n Implemented functionnality: Server side built using Symfony, search engine on client side.",
        gitUrl:"https://gitlab.com/jldjld/replace",
        projectUrl:"",
    },
    {
        cname:"work col-md-4 mt-3 ml-3 mr-3",
        cardImg:panpan,
        cardTitle:"PanPan Game",
        cardCategory:"Training project",
        cardDate:"Native Javascript / July 2019",
        cardDescription:"First school project, develop a game using native Javascript.",
        gitUrl:"https://gitlab.com/jldjld/jeu-js",
        projectUrl:"https://panpanjs.herokuapp.com/",
    }
];

export default projects;